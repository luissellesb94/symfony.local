'use strict'

function addToCart(producto)
{
    let url = 'http://symfony.local/carrito/add';
    fetch(url, {
        method : 'POST',
        headers : {'Content-Type': 'application/json'},
        body : JSON.stringify({'productos': producto })
    }).then(response => {
        if(response.ok)
            location.reload();
        else
            alert("El producto no ha podido ser añadido");
    })
}