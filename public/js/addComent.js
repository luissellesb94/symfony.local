'use strict'

function addComment(idProd)
{
    let url = 'http://symfony.local/comentario/'+idProd;
    let texto = document.getElementById('comment').value;

    fetch(url, {
        method : 'POST',
        headers : {'Content-Type': 'application/json'},
        body : JSON.stringify({'data': texto })
    }).then(response => {
        if(response.ok)
            location.reload();
        else
            alert("El comentario no ha podido ser añadido");
    })
}