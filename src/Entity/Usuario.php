<?php

namespace App\Entity;

use Captcha\Bundle\CaptchaBundle\Validator\Constraints as CaptchaAssert;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El usuario no puede estar vacio");
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="La contraseña no puede estar vacia.")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El correo no puede estar vacio")
     * @Assert\Email(message="El email '{{ value }}' no es valido")
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_imagenes;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Producto",
     *     mappedBy="usuario"
     * )
     * @ORM\JoinColumn()
     */
    private $productos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={"image/png", "image/jpg", "image/webp", "image/jpeg"}, groups={"create"})
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Carrito",
     *     mappedBy="usuario")
     * @ORM\JoinColumn()
     */
    private $carrito;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comentarios",
     *      mappedBy="usuario")
     * @ORM\JoinColumn()
     */
    private $comentarios;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "activo"})
     * @Assert\Choice(choices={"activo","bloqueado"},
     *     message="El estado debe ser 'activo' o 'bloqueado'")
     */
    private $estado;

    /**
     * @CaptchaAssert\ValidCaptcha(message="El CAPTCHA no es correcto!", groups={"create"})
     */
    private $captchaCode;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mensajes", mappedBy="userenvia")
     * @ORM\JoinColumn()
     */
    private $emisor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mensajes", mappedBy="userrecibe")
     * @ORM\JoinColumn()
     */
    private $receptor;

    /**
     * @return mixed
     */
    public function getCaptchaCode()
    {
        return $this->captchaCode;
    }

    /**
     * @param mixed $captchaCode
     */
    public function setCaptchaCode($captchaCode): void
    {
        $this->captchaCode = $captchaCode;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }



    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * @param mixed $productos
     */
    public function setProductos($productos): void
    {
        $this->productos = $productos;
    }



    public function getNumImagenes(): ?int
    {
        return $this->num_imagenes;
    }

    public function setNumImagenes(int $num_imagenes): self
    {
        $this->num_imagenes = $num_imagenes;

        return $this;
    }


    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarrito()
    {
        return $this->carrito;
    }

    /**
     * @param mixed $carrito
     */
    public function setCarrito($carrito): void
    {
        $this->carrito = $carrito;
    }

    /**
     * @return mixed
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param mixed $comentarios
     */
    public function setComentarios($comentarios): void
    {
        $this->comentarios = $comentarios;
    }

    /**
     * @return mixed
     */
    public function getEmisor()
    {
        return $this->emisor;
    }

    /**
     * @param mixed $emisor
     */
    public function setEmisor($emisor): void
    {
        $this->emisor = $emisor;
    }

    /**
     * @return mixed
     */
    public function getReceptor()
    {
        return $this->receptor;
    }

    /**
     * @param mixed $receptor
     */
    public function setReceptor($receptor): void
    {
        $this->receptor = $receptor;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [$this->getRole()];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->role,
            $this->avatar
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list($this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->role,
            $this->avatar
            )
            = unserialize($serialized);
    }
}
