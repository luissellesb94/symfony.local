<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarritoRepository")
 */
class Carrito
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario",
     *     inversedBy="carrito")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto",
     *     inversedBy="carrito")
     * @ORM\JoinColumn(nullable=false)
     */
    private $productos;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario): void
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * @param mixed $productos
     */
    public function setProductos($productos): void
    {
        $this->productos = $productos;
    }
}
