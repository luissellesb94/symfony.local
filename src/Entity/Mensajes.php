<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MensajesRepository")
 */
class Mensajes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $texto;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="emisor")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userenvia;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="receptor")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userrecibe;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexto(): ?string
    {
        return $this->texto;
    }

    public function setTexto(string $texto): self
    {
        $this->texto = $texto;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserenvia()
    {
        return $this->userenvia;
    }

    /**
     * @param mixed $userenvia
     */
    public function setUserenvia($userenvia): void
    {
        $this->userenvia = $userenvia;
    }

    /**
     * @return mixed
     */
    public function getUserrecibe()
    {
        return $this->userrecibe;
    }

    /**
     * @param mixed $userrecibe
     */
    public function setUserrecibe($userrecibe): void
    {
        $this->userrecibe = $userrecibe;
    }
}
