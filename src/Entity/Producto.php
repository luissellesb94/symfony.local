<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductoRepository")
 */
class Producto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\File(mimeTypes={"image/png", "image/jpg", "image/webp", "image/jpeg"}, groups={"create"})
     * @Assert\NotBlank(message="La imagen no puede ser nulo", groups={"create"})
     */
    private $urlNombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El nombre no puede estar vacia.", groups={"create"})
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Alt no puede estar vacio.", groups={"create"})
     */
    private $alt;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="El precio no puede ser nulo", groups={"create"})
     * @Assert\GreaterThan(
     *     value=0,
     *     message="El precio ha de ser mayor de 0.")
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="productos")
     * @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     *
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Carrito",
     *     mappedBy="productos")
     * @ORM\JoinColumn()
     */
    private $carrito;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comentarios"
     *      , mappedBy="producto")
     * @ORM\JoinColumn()
     */
    private $comentarios;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrlNombre(): ?string
    {
        return $this->urlNombre;
    }

    public function setUrlNombre(string $urlNombre = null): self
    {
        $this->urlNombre = $urlNombre;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre = null): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt = null): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    public function setPrecio(int $precio = null): self
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return mixed
     *
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarrito()
    {
        return $this->carrito;
    }

    /**
     * @param mixed $carrito
     */
    public function setCarrito($carrito): void
    {
        $this->carrito = $carrito;
    }

    /**
     * @return mixed
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param mixed $comentarios
     */
    public function setComentarios($comentarios): void
    {
        $this->comentarios = $comentarios;
    }
}
