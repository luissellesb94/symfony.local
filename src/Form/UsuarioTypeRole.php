<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 13/02/19
 * Time: 20:23
 */

namespace App\Form;


use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioTypeRole extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('role', ChoiceType::class, [
                'choices' => ['ROLE_ADMIN' => 'ROLE_ADMIN','ROLE_USER' => 'ROLE_USER']
            ])
            ->add('estado', ChoiceType::class, [
                'choices' => ['ACTIVO' => 'activo', 'BLOQUEADO' => 'bloqueado']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}