<?php

namespace App\Form;

use App\Entity\Usuario;
use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email', RepeatedType::class, [
                'type' => EmailType::class,
                'invalid_message' => 'Los correos deben coincidir',
                'options' => ['attr' => ['class' => 'email-field']],
                'required' => true,
                'first_options'  => ['label' => 'Email'],
                'second_options' => ['label' => 'Repite Email']
            ])
            ->add('password', RepeatedType::class, [
                'mapped' => true,
                'type' => PasswordType::class,
                'invalid_message' => 'Las contraseñas deben coincidir',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Contraseña'],
                'second_options' => ['label' => 'Repite Contraseña']
            ])
            ->add('avatar', FileType::class, array('data_class' => null, 'required' => false))
            ->add('captchaCode', CaptchaType::class, array(
                'captchaConfig' => 'RegisterCaptcha'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
