<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 15/02/19
 * Time: 16:18
 */

namespace App\Repository;


use App\Controller\UsuarioController;
use App\Entity\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AdminRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    public function findAllUsersExceptMe($id)
    {
        $qb = $this->createQueryBuilder('usuario')
            ->addSelect('usuario')
            ->where('usuario.id != :id')
            ->setParameter(':id',$id)
            ->getQuery();

        return $qb->execute();
    }
}