<?php

namespace App\Repository;

use App\Entity\Mensajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Mensajes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mensajes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mensajes[]    findAll()
 * @method Mensajes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MensajesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Mensajes::class);
    }

    public function findMyChats($me)
    {
        $qb = $this->createQueryBuilder('mensajes')
            ->addSelect('usuarioemisor')
            ->addSelect('usuarioreceptor')
            ->innerJoin('mensajes.userenvia', 'usuarioemisor', 'mensajes.userenvia = usuarioemisor.emisor')
            ->innerJoin('mensajes.userrecibe','usuarioreceptor', 'mensajes.userrecibe = usuarioreceptor.receptor')
            ->where('mensajes.userenvia = :id')
            ->orWhere('mensajes.userrecibe = :id')
            ->setParameter(':id', $me)
            ->getQuery();

        return $qb->execute();
    }
    // /**
    //  * @return Mensajes[] Returns an array of Mensajes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mensajes
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
