<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends AbstractController
{
    /**
     * @Route("/lucky/number/{inicio}/{fin}",
     *     name="app_lucky_number",
     *     requirements={
            "inicio"="\d+",
     *     "fin"="\d+"
     *     },
     *     host="symfony.local"
     * )
     */
    public function number($inicio=1, $fin=100)
    {
        $number = mt_rand($inicio,$fin);
        return $this->render('number.html.twig',
            [
                'number' => $number
            ]);
    }
}
