<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 14/02/19
 * Time: 19:47
 */

namespace App\Controller;


use App\Entity\Comentarios;
use App\Entity\Producto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ComentarioController extends AbstractController
{
    /**
     * @Route("/comentario/{id}", name="app_comentario_add", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function add(Request $request, Producto $producto)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $texto = $data['data'];


        if(isset($texto))
        {
            $comentario = new Comentarios();
            $comentario->setUsuario($user);
            $comentario->setProducto($producto);
            $comentario->setTexto($texto);
            $comentario->setFecha(new \DateTime('now'));

            $em->persist($comentario);
            $em->flush();

            return JsonResponse::create("ok", 200);
        }

        return JsonResponse::create("error", 400);
    }
}