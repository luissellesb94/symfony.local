<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/02/19
 * Time: 15:43
 */

namespace App\Controller;


use App\Entity\Usuario;
use App\Form\UsuarioTypeAvatar;
use App\Form\UsuarioTypeMail;
use App\Form\UsuarioTypePass;
use App\Repository\MensajesRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioController extends AbstractController
{
    /**
     * @Route("/perfil", name="app_usuario_perfil")
     * @Template("usuario/perfil.html.twig")
     */
    public function showProfile()
    {
        return [
            'usuario' => $this->getUser()
        ];
    }

    /**
     * @Route("/perfil/{id}/editar_contrasena", name="app_usuario_editar_contrasena", methods={"GET","POST"})
     * @Template("usuario/edit.html.twig")
     */
    public function editContrasena(Request $request, Usuario $usuario, UserPasswordEncoderInterface $passwordEncoder)
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY') === true)
            throw new AccessDeniedHttpException('Access Denied');

        if($this->getUser()->getId() !== $usuario->getId())
            throw new AccessDeniedHttpException('Access Denied');

        $form = $this->createForm(UsuarioTypePass::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario->setPassword(
                $passwordEncoder->encodePassword(
                    $usuario,
                    $form->get('password')->getData()
                )
            );

            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('app_usuario_perfil');
        }

        return [
            'usuario' => $usuario,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/perfil/{id}/editar_avatar", name="app_usuario_editar_avatar", methods={"GET","POST"})
     * @Template("usuario/edit.html.twig")
     */
    public function editAvatar(Request $request, Usuario $usuario)
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY') === true)
            throw new AccessDeniedHttpException('Access Denied');

        if($this->getUser()->getId() !== $usuario->getId())
            throw new AccessDeniedHttpException('Access Denied');

        $form = $this->createForm(UsuarioTypeAvatar::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $usuario->setAvatar($this->insertImgDb($form));
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('app_usuario_perfil');
        }

        return [
            'usuario' => $usuario,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/perfil/{id}/editar_correo", name="app_usuario_editar_correo", methods={"GET","POST"})
     * @Template("usuario/edit.html.twig")
     */
    public function editCorreo(Request $request, Usuario $usuario)
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY') === true)
            throw new AccessDeniedHttpException('Access Denied');

        if($this->getUser()->getId() !== $usuario->getId())
            throw new AccessDeniedHttpException('Access Denied');

        $form = $this->createForm(UsuarioTypeMail::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('app_usuario_perfil');
        }

        return [
            'usuario' => $usuario,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/perfil/mensajes", name="app_usuario_mensajes", methods={"GET"})
     * @Template("usuario/mensajes.html.twig")
     */
    public function mensajes(MensajesRepository $mensajesRepository)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $mensajes = $mensajesRepository->findMyChats($this->getUser());
        $openChats = [];
        for ($i=0; $i<sizeof($mensajes); $i++)
        {
            if($this->getUser() === $propertyAccessor->getValue($mensajes[$i], 'userenvia'))
            {
                if(!in_array($propertyAccessor->getValue($mensajes[$i], 'userrecibe'), $openChats))
                    array_push($openChats, $propertyAccessor->getValue($mensajes[$i], 'userrecibe'));
            }
            else
            {
                if(!in_array($propertyAccessor->getValue($mensajes[$i], 'userenvia'), $openChats))
                    array_push($openChats, $propertyAccessor->getValue($mensajes[$i], 'userenvia'));
            }
        }

        return [
            'mensajes' => $mensajesRepository->findMyChats($this->getUser()),
            'usuario' => $this->getUser(),
            'openChats' => $openChats
        ];
    }

    public function insertImgDb($form)
    {
        /**
         * @var  UploadedFile $file
         */
        $file = $form->get('avatar')->getData();

        $fileName = $this->generateUniqueFilename().'.'.$file->guessExtension();

        try {
            $file->move(
                $this->getParameter('products_directory'),
                $fileName
            );
        }
        catch (FileException $e) {

        }

        return $fileName;
    }

    /**
     * @return string
     */
    private function generateUniqueFilename()
    {
        return md5(uniqid());
    }
}