<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 15/02/19
 * Time: 20:20
 */

namespace App\Controller;


use App\Entity\Mensajes;
use App\Repository\UsuarioRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MensajeController extends AbstractController
{
    /**
     * @Route("/mensaje/", name="app_mensaje_send", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function sendMessage(Request $request, UsuarioRepository $usuarioRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $sender = $this->getUser();
        $data = json_decode($request->getContent(), true);

        if(isset($data) && !empty($data))
        {
            $mensaje = new Mensajes();
            $mensaje->setUserrecibe($usuarioRepository->find($data['destinatario']));
            $mensaje->setUserenvia($this->getUser());
            $mensaje->setTexto($data['mensaje']);
            $mensaje->setFecha(new \DateTime('now'));

            $em->persist($mensaje);
            $em->flush();

            return JsonResponse::create("ok", 200);
        }

        return JsonResponse::create("error", 400);
    }
}