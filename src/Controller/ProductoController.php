<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Form\ProductoType;
use App\Repository\CarritoRepository;
use App\Repository\ComentariosRepository;
use App\Repository\ProductoRepository;
use App\Repository\UsuarioRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig_Environment;

/**
 * @Route("/")
 */
class ProductoController extends AbstractController
{
    /**
     * @Route("/", name="producto_index", methods={"GET"})
     * @Template("inicio.html.twig")
     */
    public function index(ProductoRepository $productoRepository)
    {
        return [
            'productos' => $productoRepository->findAll(),
            'user' => $this->getUser()
        ];
    }

    /**
     * @Route("/producto/new", name="app_producto_new", methods={"GET","POST"})
     * @Template("producto/new.html.twig")
     */
    public function new(Request $request)
    {
        $producto = new Producto();
        $form = $this->createForm(ProductoType::class,
            $producto,
            ["validation_groups" => "create"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var  UploadedFile $file
             */
            $file = $form->get('urlNombre')->getData();
            $fileName = $this->generateUniqueFilename().'.'.$file->guessExtension();

            try {
                $file->move(
                    $this->getParameter('products_directory'),
                    $fileName
                );
            }
            catch (FileException $e) {

            }

            $producto->setUrlNombre($fileName);
            $producto->setUsuario($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($producto);
            $entityManager->flush();

            return $this->redirectToRoute('producto_index');
        }

        return [
            'producto' => $producto,
            'form' => $form->createView(),
        ];
    }

    public function insertImgDb($form)
    {
        /**
         * @var  UploadedFile $file
         */
        $file = $form->get('urlNombre')->getData();
        $fileName = $this->generateUniqueFilename().'.'.$file->guessExtension();

        try {
            $file->move(
                $this->getParameter('products_directory'),
                $fileName
            );
        }
        catch (FileException $e) {

        }

        return $fileName;
    }

    /**
     * @return string
     */
    private function generateUniqueFilename()
    {
        return md5(uniqid());
    }

    /**
     * @Route("/producto/{id}", name="producto_show", methods={"GET"})
     */
    public function show(Producto $producto,
                         ComentariosRepository $comentariosRepository,
                         CarritoRepository $carritoRepository): Response
    {
        $session = $this->get('session');

        if($this->getUser())
            $session->set('carrito', $carritoRepository->findMyCart($this->getUser()->getId()));

        return $this->render('producto/show.html.twig', [
            'producto' => $producto,
            'usuario' => $this->getUser(),
            'comentarios' => $comentariosRepository->findProductComents($producto)
        ]);
    }

    /**
     * @Route("/producto/{id}/edit", name="producto_edit", methods={"GET","POST"})
     * @Template("producto/edit.html.twig")
     */
    public function edit(Request $request, Producto $producto, ProductoRepository $productoRepository)
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY') === true)
            throw new AccessDeniedHttpException('Access Denied');

        $user = $this->getUser()->getId();
        $productoid = $producto->getUsuario()->getId();

        if($user !== $productoid && $this->getUser()->getRole() !== 'ROLE_ADMIN')
            throw new AccessDeniedHttpException('Access Denied');

        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if($form->get('urlNombre')->getData() !== null)
                $producto->setUrlNombre($this->insertImgDb($form));
            else
                $producto->setUrlNombre($imginicial);
            $producto->setUsuario($this->getUser());

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('producto_index', [
                'id' => $producto->getId(),
            ]);
        }

        return [
            'producto' => $producto,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/producto/{id}", name="producto_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Producto $producto): Response
    {
        if ($this->isCsrfTokenValid('delete'.$producto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($producto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('producto_index');
    }

    /**
     * @Route("/producto/{id}/mensaje", name="app_producto_mensaje", methods={"GET"})
     * @Template("producto/mensaje.html.twig")
     */
    public function mensaje(Producto $producto)
    {
        return [
            'producto' => $producto
        ];
    }
}
