<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new Usuario();
        $form = $this->createForm(RegistrationFormType::class, $user, ["validation_groups" => "create"]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain passwor
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $user->setEmail($form->get('email')->getData());
            $user->setRole('ROLE_USER')->setNumImagenes(0)->setEstado('activo');

            if($form->get('avatar') === null)
                $user->setAvatar('imgPerfil.png');
            else
                $user->setAvatar($this->insertImgDb($form));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('producto_index');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView()
        ]);
    }

    public function insertImgDb($form)
    {
        /**
         * @var  UploadedFile $file
         */
        $file = $form->get('avatar')->getData();
        $fileName = $this->generateUniqueFilename().'.'.$file->guessExtension();

        try {
            $file->move(
                $this->getParameter('products_directory'),
                $fileName
            );
        }
        catch (FileException $e) {

        }

        return $fileName;
    }

    /**
     * @return string
     */
    private function generateUniqueFilename()
    {
        return md5(uniqid());
    }
}
