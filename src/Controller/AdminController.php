<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 15/02/19
 * Time: 16:09
 */

namespace App\Controller;


use App\Entity\Usuario;
use App\Form\UsuarioTypeRole;
use App\Repository\AdminRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/users", name="app_admin_users", methods={"GET"})
     * @Template("admin/index.html.twig")
     */
    public function showUsers(AdminRepository $adminRepository)
    {
        return [
            'usuarios' => $adminRepository->findAllUsersExceptMe($this->getUser()->getId())
        ];
    }

    /**
     * @Route("/admin/edit/user/{id}", name="app_admin_user_edit", methods={"GET","POST"})
     * @Template("admin/edit.html.twig")
     */
    public function editUser(Request $request, Usuario $usuario)
    {
        if($this->denyAccessUnlessGranted('IS_AUTHENTICATED_ANONYMOUSLY') === true)
            throw new AccessDeniedHttpException('Access Denied');

        if($this->getUser()->getRole() !== 'ROLE_ADMIN')
            throw new AccessDeniedHttpException('Access Denied');

        $form = $this->createForm(UsuarioTypeRole::class, $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('app_admin_users');
        }

        return [
            'usuario' => $usuario,
            'form' => $form->createView()
        ];
    }
}