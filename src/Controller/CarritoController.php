<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 14/02/19
 * Time: 15:41
 */

namespace App\Controller;


use App\Entity\Carrito;
use App\Entity\Producto;
use App\Repository\CarritoRepository;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarritoController extends AbstractController
{
    /**
     * @Route("/carrito/add", name="app_carrito_add",
     *     methods={"POST"})
     */
    public function addToCart(Request $request, ProductoRepository $productoRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $user = $this->getUser();
        $producto = $productoRepository->findPerId($data['productos']);


        if(isset($producto))
        {
            $carrito = new Carrito();
            $carrito->setUsuario($user);
            $carrito->setProductos($producto[0]);

            $em->persist($carrito);
            $em->flush();

            return JsonResponse::create("ok", 200);
        }

        return JsonResponse::create("error", 400);
    }

    /**
     * @Route("/carrito/", name="app_carrito_show", methods={"GET"})
     * @Template("carrito/index.html.twig")
     */

    public function showCarrito(CarritoRepository $carritoRepository)
    {
        return [
            'carritos' => $carritoRepository->findMyCart($this->getUser()->getId())
        ];
    }
}