<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 15/02/19
 * Time: 17:58
 */

namespace App\Security;

use App\Entity\Usuario as AppUser;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CheckUsuario implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser)
        {
            return;
        }

        if($user->getEstado() === "bloqueado")
        {
            throw new CustomUserMessageAuthenticationException(
                'Tu cuenta ha sido bloqueada, lo siento'
            );
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if(!$user instanceof AppUser)
            return;

    }
}