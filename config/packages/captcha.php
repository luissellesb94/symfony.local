<?php
if (!class_exists('CaptchaConfiguration')) { return; }

// BotDetect PHP Captcha configuration options

return [
    // Captcha configuration for example page
    'RegisterCaptcha' => [
        'UserInputID' => 'captchaCode',
        'CodeLength' =>
            CaptchaRandomization::GetRandomCodeLength(4,7),
        'ImageWidth' => 250,
        'ImageHeight' => 50,
    ],
    'ExampleCaptcha' => [
        'UserInputID' => 'captchaCode',
        'ImageWidth' => 250,
        'ImageHeight' => 50,
    ]
];